#include "stdio.h"
#include <thread>
#include "LpmsSensorI.h"
#include "LpmsSensorManagerI.h"
using namespace std;

LpmsSensorManagerI* manager = LpmsSensorManagerFactory();
LpmsSensorI* lpms;

// id >= 0
void setSensorID(int id) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_OPENMAT_ID, id);
}

/*
#define SELECT_FM_GYRO_ONLY         0
#define SELECT_FM_GYRO_ACC          1
#define SELECT_FM_GYRO_ACC_MAG      2
#define SELECT_FM_MADGWICK_GYRO_ACC 3
#define SELECT_FM_MADGWICK_GYRO_ACC_MAG 4
*/
void setFilterMode(int mode) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_FILTER_MODE, mode);
}

/*
#define SELECT_GYR_RANGE_125DPS     125
#define SELECT_GYR_RANGE_245DPS     245
#define SELECT_GYR_RANGE_250DPS     250
#define SELECT_GYR_RANGE_500DPS     500 
#define SELECT_GYR_RANGE_1000DPS    1000 
#define SELECT_GYR_RANGE_2000DPS    2000
*/
void setGyroRange(int range) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_GYR_RANGE, range);
}

/*
#define SELECT_ACC_RANGE_2G     2
#define SELECT_ACC_RANGE_4G     4
#define SELECT_ACC_RANGE_8G     8
#define SELECT_ACC_RANGE_16G    16
*/
void setAccRange(int range) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_ACC_RANGE, range);
}

/*
#define SELECT_MAG_RANGE_4GAUSS     4
#define SELECT_MAG_RANGE_8GAUSS     8
#define SELECT_MAG_RANGE_12GAUSS    12
#define SELECT_MAG_RANGE_16GAUSS    16
*/
void setMagRange(int range) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_MAG_RANGE, range);
}

/*
#define SELECT_STREAM_FREQ_5HZ          5
#define SELECT_STREAM_FREQ_10HZ         10
#define SELECT_STREAM_FREQ_25HZ         25
#define SELECT_STREAM_FREQ_50HZ         50
#define SELECT_STREAM_FREQ_100HZ        100
#define SELECT_STREAM_FREQ_200HZ        200
#define SELECT_STREAM_FREQ_400HZ        400
#define SELECT_STREAM_FREQ_800HZ        800
*/
void setSamplingRate(int rate) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_SAMPLING_RATE, rate);
}

/*
#define SELECT_LPMS_QUAT_OUTPUT_ENABLED                 0x1
#define SELECT_LPMS_EULER_OUTPUT_ENABLED                (0x1 << 1)
#define SELECT_LPMS_LINACC_OUTPUT_ENABLED               (0x1 << 2)
#define SELECT_LPMS_PRESSURE_OUTPUT_ENABLED             (0x1 << 3)
#define SELECT_LPMS_GYRO_OUTPUT_ENABLED                 (0x1 << 4)
#define SELECT_LPMS_ACC_OUTPUT_ENABLED                  (0x1 << 5)
#define SELECT_LPMS_MAG_OUTPUT_ENABLED                  (0x1 << 6)
#define SELECT_LPMS_GYRO_TEMP_OUTPUT_ENABLED            (0x1 << 7)
#define SELECT_LPMS_TEMPERATURE_OUTPUT_ENABLED          (0x1 << 8)
#define SELECT_LPMS_ALTITUDE_OUTPUT_ENABLED             (0x1 << 9)
#define SELECT_LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED     (0x1 << 10)
*/
void setOutputData(int data) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_SELECT_DATA, data);
}

/*
#define SELECT_IMU_SLOW     0
#define SELECT_IMU_MEDIUM   1
#define SELECT_IMU_FAST     2
#define SELECT_IMU_DYNAMIC  3   
*/
void setMagneticCorrection(int mode) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_PARAMETER_SET, SELECT_IMU_SLOW);
}

/*
#define SELECT_LPMS_LIN_ACC_COMP_MODE_OFF       0
#define SELECT_LPMS_LIN_ACC_COMP_MODE_WEAK      1
#define SELECT_LPMS_LIN_ACC_COMP_MODE_MEDIUM    2
#define SELECT_LPMS_LIN_ACC_COMP_MODE_STRONG    3
#define SELECT_LPMS_LIN_ACC_COMP_MODE_ULTRA     4
*/
void setLinAccCompensationMode(int mode) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_LIN_ACC_COMP_MODE, mode);
}

/*
#define SELECT_LPMS_CENTRI_COMP_MODE_OFF        0
#define SELECT_LPMS_CENTRI_COMP_MODE_ON         1
*/
void setRotationalAccCompensation(int mode) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_CENTRI_COMP_MODE, mode);
}

/*
#define SELECT_LPMS_LPBUS_DATA_MODE_32          0
#define SELECT_LPMS_LPBUS_DATA_MODE_16          1
*/
void setLpBusMode(int mode) {
    this_thread::sleep_for(chrono::milliseconds(100));
    lpms->setConfigurationPrm(PRM_LPBUS_DATA_MODE, mode);
}

int main(int argc, char *argv[])
{
    ImuData d;

    // Connect to sensor
    lpms = manager->addSensor(DEVICE_LPMS_B2, "00:00:00:00:00:00");    // << change sensor id here

    // wait for sensor connection
    while (lpms->getConnectionStatus() != SENSOR_CONNECTION_CONNECTED )
        this_thread::sleep_for(chrono::milliseconds(100));

    // Set sensor ID
    setSensorID(1);

    // Select sensor filter mode
    setFilterMode(SELECT_FM_GYRO_ACC);
    
    // Set Gyro range
    setGyroRange(SELECT_GYR_RANGE_2000DPS);

    // Set Acc range
    setAccRange(SELECT_ACC_RANGE_4G);

    // Set Magnetometer range
    setMagRange(SELECT_MAG_RANGE_16GAUSS);

    // Set Streaming frequency
    setSamplingRate(SELECT_STREAM_FREQ_100HZ);

    // Set output data
    setOutputData(
        SELECT_LPMS_GYRO_OUTPUT_ENABLED |
        SELECT_LPMS_ACC_OUTPUT_ENABLED |
        SELECT_LPMS_MAG_OUTPUT_ENABLED |
        SELECT_LPMS_QUAT_OUTPUT_ENABLED | 
        SELECT_LPMS_EULER_OUTPUT_ENABLED);

    // Set magnetic correction
    setMagneticCorrection(SELECT_IMU_DYNAMIC);

    // Set linear acc compensation mode
    setLinAccCompensationMode(SELECT_LPMS_LIN_ACC_COMP_MODE_MEDIUM);

    // Set Rotational acc compensation mode
    setRotationalAccCompensation(SELECT_LPMS_CENTRI_COMP_MODE_ON);

    // Set data transfer data bit mode (32/16 bit)
    setLpBusMode(SELECT_LPMS_LPBUS_DATA_MODE_32);

    while (lpms->getConnectionStatus() == SENSOR_CONNECTION_CONNECTED
        && lpms->getSensorStatus() == SENSOR_STATUS_CALIBRATING) {
        this_thread::sleep_for(chrono::milliseconds(100));
    }

    // Save parameters to sensor
    lpms->saveCalibrationData();
    this_thread::sleep_for(chrono::milliseconds(500));

    cout << "Parameter settings done." << endl;
    cout << "Terminating program" << endl;

    // Removes the initialized sensor
    manager->removeSensor(lpms);

    // Deletes LpmsSensorManager object 
    delete manager;

    return 0;
}
