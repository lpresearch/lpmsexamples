// TestUnitt.cpp : Defines the entry point for the console application.
//

#include <cstdio>
#include <iostream>
#include <string>
#include <thread>

#ifdef _WIN32
#include <windows.h>
#include <ws2bth.h>
#include "BluetoothAPIs.h"
#include "LpmsSensorI.h"
#include "LpmsSensorManagerI.h"
#endif
#ifdef __GNUC__
#include "lpsensor/LpmsSensorI.h"
#include "lpsensor/LpmsSensorManagerI.h"
#endif


using namespace std;

LpmsSensorManagerI* manager;

// Forward Declarations
void printMenu();
void sensorDiscoveryTask();
void printDiscoveredSensorList();

void printMenu()
{
    cout << "-----------\n";
    cout << "Main menu\n";
    cout << "-----------\n";
    cout << "[d] Start discovery\n";
    cout << "[s] Stop discovery\n";
    cout << "[q] quit\n";
    cout << "-----------\n";
}

void sensorDiscoveryTask()
{
    // Blocking procedure
    cout << "Starting sensor discovery...\n";
    manager->startListDevices(false);
    while (manager->listDevicesBusy() == true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    cout << "Sensor discovery done\n";

    printDiscoveredSensorList();
}

string getDeviceTypeName(int type)
{
    if (type == DEVICE_LPMS_B)
        return "LPMS-B";
    else if (type == DEVICE_LPMS_U)
        return "LPMS-U";
    else if (type == DEVICE_LPMS_C)
        return "LPMS-CU";
    else if (type == DEVICE_LPMS_RS232)
        return "LPMS-RS232";
    else if (type == DEVICE_LPMS_B2)
        return "LPMS-B2";
    else if (type == DEVICE_LPMS_U2)
        return "LPMS-U2";
    else if (type == DEVICE_LPMS_C2)
        return "LPMS-CU2";
}

void printDiscoveredSensorList()
{
    LpmsDeviceList deviceList = manager->getDeviceList();

    cout << "------------------------------------\n";
    cout << "List of detected devices\n";
    cout << "------------------------------------\n";
    for (int i = 0; i < deviceList.nDevices; ++i)
    {
        cout << "[" << i << "] Mac: " << deviceList.getDeviceId(i) << " Device Type: " << getDeviceTypeName(deviceList.getDeviceType(i)) << endl;
    }

    cout << "------------------------------------\n";
}

int main(int argc, char *argv[])
{
    std::thread* t1=NULL;
    // Gets a LpmsSensorManager instance
    manager = LpmsSensorManagerFactory();
    //std::this_thread::sleep_for(std::chrono::milliseconds(500));

    string cmd;
    bool quit = false;
    while (!quit)
    {
        printMenu();
        cout << "Enter command: ";
        getline(cin, cmd);
        switch (cmd[0])
        {
            // Discover
        case 'd':
            manager->stopListDevices();
            if (t1 != NULL)
                t1->join();
            t1 = new thread(sensorDiscoveryTask);
            break;

        case 's':
            manager->stopListDevices();
            t1->join();
            t1 = NULL;
            break;

        case 'q':
            quit = true;
            break;

        default:
            break;
        }

    }

    if ( t1 != NULL)
        t1->join();

    // Deletes LpmsSensorManager object 
    delete manager;

    return 0;
}