#include "stdio.h"
#include <thread>
#include "LpmsSensorI.h"
#include "LpmsSensorManagerI.h"
using namespace std;

int main(int argc, char *argv[])
{
    ImuData d;

    // Gets a LpmsSensorManager instance
    LpmsSensorManagerI* manager = LpmsSensorManagerFactory();

    // Connect to sensor
    LpmsSensorI* lpms = manager->addSensor(DEVICE_LPMS_B2, "00:00:00:00:00:00");    // << change sensor id here

    // wait for sensor connection
    while (lpms->getConnectionStatus() != SENSOR_CONNECTION_CONNECTED)
        this_thread::sleep_for(chrono::milliseconds(1));
    
    while (1) {
        // Checks, if conncted
        if (
            lpms->getConnectionStatus() == SENSOR_CONNECTION_CONNECTED &&
            lpms->hasImuData()
            ) {

            // Reads quaternion data
            d = lpms->getCurrentData();

            // Shows data
            printf("Timestamp=%.3f, qW=%.3f, qX=%.3f, qY=%.3f, qZ=%.3f, x=%.3f, y=%.3f, z=%.3f\n",
                d.timeStamp, d.q[0], d.q[1], d.q[2], d.q[3],
                d.r[0], d.r[1], d.r[2]
                );
        }
    }
    
    // Removes the initialized sensor
    manager->removeSensor(lpms);

    // Deletes LpmsSensorManager object 
    delete manager;

    return 0;
}
